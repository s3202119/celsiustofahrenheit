package nl.utwente.di.celsiusToFahrenheit;

public class Calculator {

    public static double celsiusToFahrenheit(double celsius) {
        return ((celsius * 9 / 5) + 32);
    }
}
